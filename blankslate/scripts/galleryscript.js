var overlay;
var overlayimg;
var overlayActive = false;


function overlayToggle()
{
    if (overlayActive)
    {
        overlay.style.visibility = "hidden";
        overlayActive = false;
    }
    else
    {
        overlay.style.visibility = "visible";
        overlayActive = true;
    }
}

function activateGallery(imgSrc)
{
    overlayimg.src = imgSrc;
    overlayToggle();
}

function scrollGallery(e)
{
    e.target.elem.scrollLeft += e.target.scrollLength;
}

function galleryInit()
{
    overlay = document.getElementById("galleryoverlay");
    overlayimg = overlay.firstChild;

    var subs = Array.from(document.getElementsByClassName("subgallery"));

    subs.forEach(e => {
        //if (e.scrollWidth > e.clientWidth)
        //{
            var next = e.parentElement.getElementsByClassName("subgallery_next")[0];
            var prev = e.parentElement.getElementsByClassName("subgallery_previous")[0];

            //next.style.visibility = "visible";
            //prev.style.visibility = "visible";

            next.addEventListener("click", scrollGallery);
            next.elem = e;
            next.scrollLength = 250;
            prev.addEventListener("click", scrollGallery);
            prev.elem = e;
            prev.scrollLength = -250;
        //}      
    });
}


galleryInit();