<!DOCTYPE html>
<html <?php  ?> <?php blankslate_schema_type(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header id="header" role="banner">
<img class="logo" src="/wp-content/uploads/2023/01/logo-smol.png" />
<nav data-lang="sv">
<?php echo strip_tags(wp_nav_menu( array( 'menu' => 'sv_menu', 'container' => false,  'echo' => false, 'items_wrap' => '%3$s', 'depth' => 0, 'echo' => false )), '<a>');?>
<a OnClick="switchLang()"><p>English</p></a>
</nav>
<nav data-lang="en">
<?php echo strip_tags(wp_nav_menu( array( 'menu' => 'en_menu', 'container' => false,  'echo' => false, 'items_wrap' => '%3$s', 'depth' => 0, 'echo' => false )), '<a>');?>
<a OnClick="switchLang()"><p>Svenska</p></a>
</nav>
</header>