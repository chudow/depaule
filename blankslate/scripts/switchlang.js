
var elems;
var defaultDisplays = [];

function switchLang()
{
    var switchTo = localStorage.getItem('lang');

    for (var i = 0; i < elems.length; i++)
    {
        if (elems[i].getAttribute("data-lang") == switchTo)
        {
            elems[i].style.display = defaultDisplays[i];
        }
        else
        {
            elems[i].style.display = "none";
        }
    }

    if (switchTo == "en") switchTo = "sv";
    else switchTo = "en";

    localStorage.setItem('lang', switchTo);
}

function hideLang(hide)
{
    for (var i = 0; i < elems.length; i++)
    {
        if (elems[i].getAttribute("data-lang") == hide)
        {
            elems[i].style.display = "none";
        }
        else
        {
            elems[i].style.display = defaultDisplays[i];
        }
    }
}

function switchLangInit()
{
    elems = document.querySelectorAll("[data-lang]");
    defaultDisplays.length = elems.length;
    var lang = localStorage.getItem('lang');

    for (var i = 0; i < elems.length; i++)
    {
        defaultDisplays[i] = window.getComputedStyle(elems[i], null).display;
    }

    if (localStorage.getItem('lang') == null)
    {
        localStorage.setItem('lang', 'en');
        lang = localStorage.getItem('lang');
        //switchLang();
    }

    hideLang(lang);
}


switchLangInit();