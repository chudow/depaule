<?php get_header(); ?>
<main>
<article>
<?php $data = get_field('section');
foreach ($data as $s)
{
    if ($s['acf_fc_layout'] == 'text_swe')
    {
        echo '<div data-lang="sv">' . $s['textblock'] . '</div>';
    }
    elseif ($s['acf_fc_layout'] == 'text_eng')
    {
        echo '<div data-lang="en">' . $s['textblock'] . '</div>';
    }
    elseif ($s['acf_fc_layout'] == 'text_all_lang')
    {
        echo '<div>' . $s['textblock'] . '</div>';
    }
    elseif ($s['acf_fc_layout'] == 'image')
    {
        echo wp_get_attachment_image($s['sectionimage']['ID']);
    }
    elseif ($s['acf_fc_layout'] == 'gallery')
    {
        
        $display_all_posts = $s['display_all_posts'];

        if($display_all_posts) {

            $furniture_posts = get_posts(array(
                'post_type' => 'furniture',
                'post_status' => 'publish',
                'posts_per_page' => -1
            ));

            foreach($furniture_posts as $furniture_post) {

                $furniture_post_id = $furniture_post->ID;
                
                $swedesc = get_field('swedesc', $furniture_post_id);
                $engdesc = get_field('engdesc', $furniture_post_id);
                $swetitle = get_field('swetitle', $furniture_post_id);
                $engtitle = get_field('engtitle', $furniture_post_id);

                $mainimage = wp_get_attachment_image_src(get_field('mainimage', $furniture_post_id)['ID'], 'large')[0];
                $mainthumbnail = wp_get_attachment_image_src(get_field('mainimage', $furniture_post_id)['ID'])[0];
                
                echo '<div class="product">';
                echo '<h2 data-lang="sv">' . $swetitle . '</h2>';
                echo '<h2 data-lang="en">' . $engtitle . '</h2>';
                echo '<div class="topgallery">';
                    //echo wp_get_attachment_image(get_field('mainimage', $furniture_post_id)['ID']);
                    echo '<img src=' . $mainthumbnail . ' onclick="activateGallery(\'' . $mainimage . '\')" />';
                    echo '<p data-lang="sv">' . $swedesc . '</p>';
                    echo '<p data-lang="en">' . $engdesc . '</p>';
                echo '</div>'; //topgallery
                echo '<div class=subgallery_container>';
                echo '<p class="subgallery_previous"> << </p>';
                echo '<div class="subgallery">';
                foreach (get_field('subimages', $furniture_post_id) as $image_row)
                {
                    //echo wp_get_attachment_image($image_row['image']['ID']);
                    $subimage = wp_get_attachment_image_src($image_row['image']['ID'], 'large')[0];
                    $subthumbnail = wp_get_attachment_image_src($image_row['image']['ID'])[0];
                    echo '<img src=' . $subthumbnail . ' onclick="activateGallery(\'' . $subimage . '\')" />';
                }
                echo '</div>';//subgallery
                echo '<p class="subgallery_next"> >> </p>';
                echo '</div>';//subgallery_container
                echo '</div>';//product
            }

        } else {

            foreach($s['furniture'] as $furniture_post_id)
            {
                $swedesc = get_field('swedesc', $furniture_post_id);
                $engdesc = get_field('engdesc', $furniture_post_id);
                $swetitle = get_field('swetitle', $furniture_post_id);
                $engtitle = get_field('engtitle', $furniture_post_id);

                $mainimage = wp_get_attachment_image_src(get_field('mainimage', $furniture_post_id)['ID'], 'large')[0];
                $mainthumbnail = wp_get_attachment_image_src(get_field('mainimage', $furniture_post_id)['ID'])[0];
                
                echo '<div class="product">';
                echo '<h2 data-lang="sv">' . $swetitle . '</h2>';
                echo '<h2 data-lang="en">' . $engtitle . '</h2>';
                echo '<div class="topgallery">';
                    //echo wp_get_attachment_image(get_field('mainimage', $furniture_post_id)['ID']);
                    echo '<img src=' . $mainthumbnail . ' onclick="activateGallery(\'' . $mainimage . '\')" />';
                    echo '<p data-lang="sv">' . $swedesc . '</p>';
                    echo '<p data-lang="en">' . $engdesc . '</p>';
                echo '</div>'; //topgallery
                echo '<div class="subgallery_container">';
                echo '<div class="subgallery">';
                foreach (get_field('subimages', $furniture_post_id) as $image_row)
                {
                    //echo wp_get_attachment_image($image_row['image']['ID']);
                    $subimage = wp_get_attachment_image_src($image_row['image']['ID'], 'large')[0];
                    $subthumbnail = wp_get_attachment_image_src($image_row['image']['ID'])[0];
                    echo '<img src=' . $subthumbnail . ' onclick="activateGallery(\'' . $subimage . '\')" />';
                }
                echo '</div>';//subgallery

                echo '</div>';//subgallery_container
                echo '</div>';//product
            }

        }
        echo '</div>';
    }
}
/*the_field('textcontent'); ?>
<?php $img = get_field('image'); ?>
<?php echo wp_get_attachment_image($img['ID']);*/ ?>
</article>
</main>
<?php get_footer(); ?>